package mindslab.ai.daekyoMediaServer.controller;

import mindslab.ai.daekyoMediaServer.service.AudioService;
import mindslab.ai.daekyoMediaServer.util.DaekyoMediaServerException;
import mindslab.ai.daekyoMediaServer.util.UserAgentParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/audio")
public class AudioController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    AudioService service;

    @RequestMapping(value="{audioName}", method= RequestMethod.GET)
    public void audio1(HttpServletRequest request, HttpServletResponse response, @PathVariable("audioName") String audioName) {
        logger.info(String.format("[%s invoked]", audioName));

        try {
            UserAgentParser.parseUserAgent(request);
        } catch(DaekyoMediaServerException e) {
            logger.error("FORBIDDEN USER", e);
            return;
        }

        try {
            service.getContentMediaAudio(request, response, audioName);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
