package mindslab.ai.daekyoMediaServer.controller;

import mindslab.ai.daekyoMediaServer.service.VideoService;
import mindslab.ai.daekyoMediaServer.util.DaekyoMediaServerException;
import mindslab.ai.daekyoMediaServer.util.UserAgentParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@Controller
@RequestMapping("/video")
public class VideoController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    VideoService service;

    @RequestMapping(value="{videoName}", method= RequestMethod.GET)
    public void video1(HttpServletRequest request, HttpServletResponse response, @PathVariable String videoName) {
        logger.info(String.format("[%s invoked]", videoName));
        try {
            UserAgentParser.parseUserAgent(request);
        } catch(DaekyoMediaServerException e) {
            logger.error("FORBIDDEN USER", e);
            return;
        }

        try {
            service.getContentMediaVideo(request, response, videoName);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

}
