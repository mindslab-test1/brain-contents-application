package mindslab.ai.daekyoMediaServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DaekyoMediaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DaekyoMediaServerApplication.class, args);
    }

}
