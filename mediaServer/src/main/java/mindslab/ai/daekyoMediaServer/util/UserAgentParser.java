package mindslab.ai.daekyoMediaServer.util;

import net.sf.uadetector.UserAgentStringParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

public class UserAgentParser {
    private static final Logger logger = LoggerFactory.getLogger(UserAgentParser.class);

    private static final String IS_MOBILE = "MOBILE";
    private static final String IS_PHONE = "PHONE";

    @Autowired
    private static UserAgentStringParser parser;

    public static void parseUserAgent(HttpServletRequest request) throws DaekyoMediaServerException {

        String userAgent = request.getHeader("User-Agent").toUpperCase();
        String userReferer = request.getHeader("referer");

        if (userAgent.indexOf(IS_MOBILE) > -1) {
            if (userAgent.indexOf(IS_PHONE) == -1) {
                logger.info("Phone Connect");
            } else {
                logger.info("TABLET Connect");
            }
        } else {
            logger.info("PC Connect");
        }

        logger.info(userReferer);

        if (!userReferer.contains("https://www.dkbooktalk.com/")) {
            Enumeration<String> headerNames = request.getHeaderNames();

            while(headerNames.hasMoreElements()) {
                String headerName = headerNames.nextElement();
                logger.error(String.format("%s : %s", headerName, request.getHeader(headerName)));
            }
            throw new DaekyoMediaServerException(HttpStatus.FORBIDDEN);
        }

    }
}
