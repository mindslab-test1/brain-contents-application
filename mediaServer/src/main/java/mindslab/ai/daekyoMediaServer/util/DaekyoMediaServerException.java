package mindslab.ai.daekyoMediaServer.util;

import org.springframework.http.HttpStatus;

public class DaekyoMediaServerException extends Exception {
    private int status;
    private String message;

    public DaekyoMediaServerException(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public DaekyoMediaServerException(HttpStatus status) {
        this.status = status.value();
        this.message = status.toString();
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
