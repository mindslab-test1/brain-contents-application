package mindslab.ai.daekyo_demo.ui.video;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import mindslab.ai.daekyo_demo.R;
import mindslab.ai.daekyo_demo.activity.media.MediaActivity;

public class VideoFragment extends Fragment {

    private VideoViewModel videoViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        videoViewModel =
                ViewModelProviders.of(this).get(VideoViewModel.class);
        View root = inflater.inflate(R.layout.fragment_video, container, false);
//        final TextView textView = root.findViewById(R.id.text_slideshow);
//        videoViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });

        ImageButton btn_video1 = (ImageButton) root.findViewById(R.id.btn_video1);
        btn_video1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        getActivity(),
                        MediaActivity.class
                );
                intent.putExtra("mediaUrl", "http://10.122.66.10:8080/video/video1");
                startActivity(intent);
            }
        });

        ImageButton btn_video2 = (ImageButton) root.findViewById(R.id.btn_video2);
        btn_video2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        getActivity(),
                        MediaActivity.class
                );
                intent.putExtra("mediaUrl", "http://10.122.66.10:8080/video/video2");
                startActivity(intent);
            }
        });

        ImageButton btn_video3 = (ImageButton) root.findViewById(R.id.btn_video3);
        btn_video3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        getActivity(),
                        MediaActivity.class
                );
                intent.putExtra("mediaUrl", "http://10.122.66.10:8080/video/video3");
                startActivity(intent);
            }
        });

        ImageButton btn_video4 = (ImageButton) root.findViewById(R.id.btn_video4);
        btn_video4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        getActivity(),
                        MediaActivity.class
                );
                intent.putExtra("mediaUrl", "http://10.122.66.10:8080/video/video4");
                startActivity(intent);
            }
        });

        ImageButton btn_video5 = (ImageButton) root.findViewById(R.id.btn_video5);
        btn_video5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        getActivity(),
                        MediaActivity.class
                );
                intent.putExtra("mediaUrl", "http://10.122.66.10:8080/video/video5");
                startActivity(intent);
            }
        });
        return root;
    }
}