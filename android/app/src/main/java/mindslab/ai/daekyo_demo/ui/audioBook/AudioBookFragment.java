package mindslab.ai.daekyo_demo.ui.audioBook;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import mindslab.ai.daekyo_demo.R;
import mindslab.ai.daekyo_demo.activity.media.MediaActivity;

public class AudioBookFragment extends Fragment {

    private AudioBookViewModel audioBookViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        audioBookViewModel =
                ViewModelProviders.of(this).get(AudioBookViewModel.class);
        View root = inflater.inflate(R.layout.fragment_audiobook, container, false);
//        final TextView textView = root.findViewById(R.id.text_gallery);
//        audioBookViewModel.getText().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });

        ImageButton btn_audio1 = (ImageButton) root.findViewById(R.id.btn_audio1);
        btn_audio1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        getActivity(),
                        MediaActivity.class
                );
                intent.putExtra("mediaUrl", "http://10.122.66.10:8080/audio/audio1");
                startActivity(intent);
            }
        });

        ImageButton btn_audio2 = (ImageButton) root.findViewById(R.id.btn_audio2);
        btn_audio2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        getActivity(),
                        MediaActivity.class
                );
                intent.putExtra("mediaUrl", "http://10.122.66.10:8080/audio/audio2");
                startActivity(intent);
            }
        });

        ImageButton btn_audio3 = (ImageButton) root.findViewById(R.id.btn_audio3);
        btn_audio3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(
                        getActivity(),
                        MediaActivity.class
                );
                intent.putExtra("mediaUrl", "http://10.122.66.10:8080/audio/audio3");
                startActivity(intent);
            }
        });

        return root;
    }
}