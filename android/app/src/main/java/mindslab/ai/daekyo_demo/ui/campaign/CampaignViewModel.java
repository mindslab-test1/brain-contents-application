package mindslab.ai.daekyo_demo.ui.campaign;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CampaignViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CampaignViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is Campaign fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}