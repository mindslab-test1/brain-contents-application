package mindslab.ai.daekyo_demo.ui.audioBook;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class AudioBookViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public AudioBookViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is AudioBook fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}